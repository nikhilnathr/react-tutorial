const initState = {
  posts: [
    {id: 1, title: "Happy Christmas", body: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempora repellat quidem quis! Rem quia, excepturi sed atque dolore odio consectetur dolor dicta neque blanditiis iusto tempora magnam pariatur nam veniam."},
    {id: 2, title: "Happy Birthday", body: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempora repellat quidem quis! Rem quia, excepturi sed atque dolore odio consectetur dolor dicta neque blanditiis iusto tempora magnam pariatur nam veniam."},
    {id: 3, title: "Happy Easter", body: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempora repellat quidem quis! Rem quia, excepturi sed atque dolore odio consectetur dolor dicta neque blanditiis iusto tempora magnam pariatur nam veniam."},
  ]
}

const rootReducer = (state = initState, action) => {
  if (action.type === "DELETE_POST") {
    let newPosts = state.posts.filter(post =>  {
      return  action.id !== post.id
    });
    return {
      ...state,
      posts: newPosts
    }
  }
  return state;
}

export default rootReducer;
