import React from 'react';

const Contact = (props) => {
  setTimeout(() =>{
    props.history.push('/about');
  }, 2000)
  return(
    <div>
      <div className="container">
        <h4 className="center">Contact</h4>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Explicabo amet consequatur hic eius facilis itaque dolore, sequi nemo blanditiis maiores! Ullam maiores, eligendi totam sit id libero nostrum, enim amet!</p>
      </div>
    </div>
  );
}

export default Contact;
