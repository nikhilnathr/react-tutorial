import React from 'react';
import Rainbow from '../hoc/Rainbow';

const About = () => {
  return(
    <div>
      <div className="container">
        <h4 className="center">About</h4>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Explicabo amet consequatur hic eius facilis itaque dolore, sequi nemo blanditiis maiores! Ullam maiores, eligendi totam sit id libero nostrum, enim amet!</p>
      </div>
    </div>
  );
}

export default Rainbow(About);
