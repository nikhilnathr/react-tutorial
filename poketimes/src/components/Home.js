import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Pokeball from '../pokeball.png';
import { connect } from 'react-redux';

class Home extends Component {
  render() {
    const  { posts }  = this.props;
    const postsList = posts.length ? (
      posts.map(post => {
        return(
          <div className="post card" key={ post.id }>
            <img src={ Pokeball } alt="A pokeball"/>
            <div className="card-content">
              <Link to={ '/'+post.id }>
                <h4 className="card-title red-text">{ post.title }</h4>
              </Link>
              <p>{ post.body }</p>
            </div>
          </div>
        );
      })
    ) : (
      <div className="center">No posts yet</div>
    );
    return(
      <div>
        <div className="container">
          <h4 className="center">Home</h4>
          { postsList }
        </div>
      </div>
    );
  }

}
const mapStatetoProps = (state) => {
  return {
    posts: state.posts
  }
}

export default connect(mapStatetoProps)(Home);
